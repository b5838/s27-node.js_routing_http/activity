let http = require("http");
let port = 4000;

http.createServer((request,response) => {
    // 2.a If the url is http://localhost:4000/, send a response Welcome to Booking System.

        if(request.url == "/" && request.method == "GET"){
            response.writeHead(200, {"Content-Type":'text/plain'});

            response.end("Welcome to Booking System");
        }

    // 2.b If the url is http://localhost:4000/profile, send a response Welcome to your profile!

        if(request.url == "/profile" && request.method == "GET"){
            response.writeHead(200, {"Content-Type":'text/plain'});

            response.end("Welcome to your profile.");
        }
    
    // 2.c If the url is http://localhost:4000/courses, send a response Here’s our courses available.

        if(request.url == "/courses" && request.method == "GET"){
            response.writeHead(200, {"Content-Type":'text/plain'});

            response.end("Here's our courses available.");
        }
    
    // 2.d If the url is http://localhost:4000/addcourse, send a response Add a course to our resources.

        if(request.url == "/addcourse" && request.method == "POST"){
            response.writeHead(200, {"Content-Type":'text/plain'});

            response.end("Add a course to our resources.");
        }

    // 2.e If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources.

        if(request.url == "/updatecourse" && request.method == "PUT"){
            response.writeHead(200, {"Content-Type":'text/plain'});

            response.end("Update a course to our resources.");
        }

    // 2.f If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources.

        if(request.url == "/archiveCourse" && request.method == "DELETE"){
            response.writeHead(200, {"Content-Type":'text/plain'});

            response.end("Archive courses to our resources.");
        }


}).listen(port);

console.log(`Server runs successfully at localhost:${port}`);